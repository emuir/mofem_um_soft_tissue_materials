#! /usr/bin/fish
echo "Build libs and install Users's Modules"
echo "make install"
echo ""
cd ../../lib
make -j 4 install;

cd ../usr
echo "cmake users_modules"
echo "cmake -DCMAKE_CXX_FLAGS="-I/Users/euan/build/mofem/usr/include -Wno-bind-to-temporary-copy -Wno-overloaded-virtual" -DCMAKE_EXE_LINKER_FLAGS=-L/Users/euan/build/mofem/usr/lib -DCMAKE_BUILD_TYPE=Release users_modules/"
echo ""
cmake -DCMAKE_CXX_FLAGS="-I/Users/euan/build/mofem/usr/include -Wno-bind-to-temporary-copy -Wno-overloaded-virtual" -DCMAKE_EXE_LINKER_FLAGS=-L/Users/euan/build/mofem/usr/lib -DCMAKE_BUILD_TYPE=Release users_modules/
echo ""
echo "Build all User's Modules"
echo "make -j 4"
echo ""
make -j 4;
