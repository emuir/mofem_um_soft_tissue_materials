# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

#include subdirectory
include_directories(${UM_SOURCE_DIR}/nonlinear_elastic_materials/src)
include_directories(${UM_SOURCE_DIR}/soft_tissue_materials/src)

if(ADOL-C_LIBRARY)
  add_executable(arc_length_soft_tissue
  ${UM_SOURCE_DIR}/soft_tissue_materials/arc_length_soft_tissue.cpp)
  target_link_libraries(arc_length_soft_tissue
    users_modules
    mofem_finite_elements
    mofem_interfaces
    mofem_multi_indices
    mofem_petsc
    users_modules_obsolete
    mofem_approx
    mofem_third_party
    mofem_cblas
    boost_program_options
  ${MoFEM_PROJECT_LIBS}
)

cm_export_file("cube.cub" export_files_arc_length_soft_tissue)

file(COPY fibres_mat_prop.txt
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE)

file(COPY h5m_to_vtk.fish
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE)

file(COPY all.fish
  DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
  FILE_PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_EXECUTE)

endif(ADOL-C_LIBRARY)
